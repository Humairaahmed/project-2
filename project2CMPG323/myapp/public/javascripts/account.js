/* display the name and surname of the user */
window.addEventListener('load', (event) => {
    //Settings the image variable
    var imagesFromBackEnd= document.querySelector("#linkToImages").textContent;

    console.log("images fro the backend" + imagesFromBackEnd);
    if(imagesFromBackEnd === "incorrect username or password")
    {
        alert("Incorrect username or password");
    }
    const myArray1 = imagesFromBackEnd.split(",");
    console.log(myArray1);
    if(myArray1.length <= 7)
    {
        document.getElementById("next").style.display = "none";
        document.getElementById("labelNext").style.display = "none";
    }
    localStorage.setItem("images", imagesFromBackEnd);
    var numberOrder = 0;
    var number = 0;
    const imageName = [];
    const caption = [];
    const date = [];
    const sharedStatus = [];
    const sharedWith = [];
    const myArray = [];
    const geoLocation = [];
    var forLoopLength = myArray1.length/7;
    for(var i = 0 ; i < forLoopLength ; i++)
    {
        if(numberOrder == 0)
        {
            myArray[i] = myArray1[number];
            numberOrder++;
            number++;
        }
        if(numberOrder == 1)
        {
            imageName[i] = myArray1[number];
            numberOrder++;
            number++;
        }
        if(numberOrder == 2)
        {
            caption[i] = myArray1[number];
            numberOrder++;
            number++;
        }
        if(numberOrder == 3)
        {
            sharedStatus[i] = myArray1[number];
            numberOrder++;
            number++;
        }
        if(numberOrder == 4)
        {
            date[i] = myArray1[number];
            numberOrder++;
            number++;
        }
        if(numberOrder == 5)
        {
            sharedWith[i] = myArray1[number];
            numberOrder++;
            number++;
        }
        if(numberOrder == 6)
        {
            geoLocation[i] = myArray1[number];
            numberOrder++;
            number++;
            numberOrder = 0;
        }
    }
    let numberOfImages = myArray.length;
    localStorage.setItem("numberOfImages", numberOfImages);
    console.log(localStorage.getItem("numberOfImages"));
    localStorage.setItem("myArray", JSON.stringify(myArray));

    localStorage.setItem("geoLocation", JSON.stringify(geoLocation));
    var retrievedDataGeoLocations = localStorage.getItem("geoLocation");
    var geoLocationArray = JSON.parse(retrievedDataGeoLocations);

    localStorage.setItem("sharedStatus", JSON.stringify(sharedStatus));
    var retrievedDataStatusStatus = localStorage.getItem("sharedStatus");
    var StatusStatusArray = JSON.parse(retrievedDataStatusStatus);

    localStorage.setItem("DatePosted", JSON.stringify(date));
    var retrievedDataDatePosted = localStorage.getItem("DatePosted");
    var DatePostedArray = JSON.parse(retrievedDataDatePosted);

    localStorage.setItem("SharedWith", JSON.stringify(sharedWith));
    var retrievedDataSharedWith = localStorage.getItem("SharedWith");
    var SharedWithArray = JSON.parse(retrievedDataSharedWith);

    localStorage.setItem("imageName", JSON.stringify(imageName));
    var retrievedDataimageName = localStorage.getItem("imageName");
    var imageNameArray = JSON.parse(retrievedDataimageName);



    var retrievedData = localStorage.getItem("myArray");
    var imagesArray = JSON.parse(retrievedData);

    localStorage.setItem("captionArray", JSON.stringify(caption));
    var retrievedDataCaption = localStorage.getItem("captionArray");
    var captionArray = JSON.parse(retrievedDataCaption);


    document.getElementById("usernameHeading").innerText = localStorage.getItem("username") ;
    localStorage.setItem("imageNum",0);

    document.getElementById("imageFromAccount").src=imagesArray[localStorage.getItem("imageNum")];
    document.getElementById("caption").innerText =captionArray[localStorage.getItem("imageNum")];
    document.getElementById("shared").innerText =StatusStatusArray[localStorage.getItem("imageNum")];
    document.getElementById("sharedWith").innerText =SharedWithArray[localStorage.getItem("imageNum")];
    document.getElementById("dateposted").innerText =DatePostedArray[localStorage.getItem("imageNum")];
    document.getElementById("geoLocation").innerText ="Location: " + geoLocationArray[localStorage.getItem("imageNum")];
    console.log(imageNameArray[localStorage.getItem("imageNum")]);
    document.getElementById("previous").style.display = "none";
    document.getElementById("labelPrevious").style.display = "none";

    localStorage.setItem("imageToShare", imagesArray[localStorage.getItem("imageNum")]);
    localStorage.setItem("imageToShare", localStorage.getItem("imageToShare").split("https:/").pop() );
});

function hideMessageAboutDownload(){
    console.log("test");
    document.getElementById("messageAboutDownload").style.display = "none";
}

function display()
{
    console.log("display");
};

function next()
{
    if (localStorage.getItem("imageNum") < (localStorage.getItem("numberOfImages")-1))
    {
        var imageNumber = localStorage.getItem("imageNum");
        imageNumber++;
        localStorage.setItem("imageNum",imageNumber);

        var retrievedData = localStorage.getItem("myArray");
        var imagesArray = JSON.parse(retrievedData);
        document.getElementById("imageFromAccount").src=imagesArray[localStorage.getItem("imageNum")];

        var retrievedDataCaption = localStorage.getItem("captionArray");
        var captionArray = JSON.parse(retrievedDataCaption);
        document.getElementById("caption").innerText =captionArray[localStorage.getItem("imageNum")];

        var retrievedDataStatusStatus = localStorage.getItem("sharedStatus");
        var StatusStatusArray = JSON.parse(retrievedDataStatusStatus);
        document.getElementById("shared").innerText =StatusStatusArray[localStorage.getItem("imageNum")];

        var retrievedDataDatePosted = localStorage.getItem("DatePosted");
        var DatePostedArray = JSON.parse(retrievedDataDatePosted);
        document.getElementById("dateposted").innerText =DatePostedArray[localStorage.getItem("imageNum")];

        var retrievedDataSharedWith = localStorage.getItem("SharedWith");
        var SharedWithArray = JSON.parse(retrievedDataSharedWith);
        document.getElementById("sharedWith").innerText =SharedWithArray[localStorage.getItem("imageNum")];

        var retrievedDataimageName = localStorage.getItem("imageName");
        var imageNameArray = JSON.parse(retrievedDataimageName);
        console.log(imageNameArray[localStorage.getItem("imageNum")]);

        var retrievedDataGeoLocations = localStorage.getItem("geoLocation");
        var geoLocationArray = JSON.parse(retrievedDataGeoLocations);
        document.getElementById("geoLocation").innerText ="Location: " + geoLocationArray[localStorage.getItem("imageNum")];

        if (localStorage.getItem("imageNum") == 1)
        {
            document.getElementById("previous").style.display = "block";
            document.getElementById("labelPrevious").style.display = "block";
        }
        var caption = localStorage.getItem("caption");
        console.log(localStorage.getItem("imageNum"));
        if (localStorage.getItem("imageNum") == (localStorage.getItem("numberOfImages")-1))
        {
            document.getElementById("next").style.display = "none";
            document.getElementById("labelNext").style.display = "none";
        }
    }

}

function previous()
{
    var imageNumber = localStorage.getItem("imageNum");
    imageNumber--;
    localStorage.setItem("imageNum",imageNumber);

    var retrievedData = localStorage.getItem("myArray");
    var imagesArray = JSON.parse(retrievedData);
    document.getElementById("imageFromAccount").src=imagesArray[localStorage.getItem("imageNum")];

    var retrievedDataCaption = localStorage.getItem("captionArray");
    var captionArray = JSON.parse(retrievedDataCaption);
    document.getElementById("caption").innerText =captionArray[localStorage.getItem("imageNum")];

    var retrievedDataStatusStatus = localStorage.getItem("sharedStatus");
    var StatusStatusArray = JSON.parse(retrievedDataStatusStatus);
    document.getElementById("shared").innerText =StatusStatusArray[localStorage.getItem("imageNum")];

    var retrievedDataDatePosted = localStorage.getItem("DatePosted");
    var DatePostedArray = JSON.parse(retrievedDataDatePosted);
    document.getElementById("dateposted").innerText =DatePostedArray[localStorage.getItem("imageNum")];

    var retrievedDataSharedWith = localStorage.getItem("SharedWith");
    var SharedWithArray = JSON.parse(retrievedDataSharedWith);
    document.getElementById("sharedWith").innerText =SharedWithArray[localStorage.getItem("imageNum")];

    var retrievedDataGeoLocations = localStorage.getItem("geoLocation");
    var geoLocationArray = JSON.parse(retrievedDataGeoLocations);
    document.getElementById("geoLocation").innerText ="Location: " + geoLocationArray[localStorage.getItem("imageNum")];

    var retrievedDataimageName = localStorage.getItem("imageName");
    var imageNameArray = JSON.parse(retrievedDataimageName);
    console.log(imageNameArray[localStorage.getItem("imageNum")]);


    if (localStorage.getItem("imageNum") == (localStorage.getItem("numberOfImages")-2))
    {
        document.getElementById("next").style.display = "block";
        document.getElementById("labelNext").style.display = "block";
    }
    var caption = localStorage.getItem("caption");
    console.log(localStorage.getItem("imageNum"));
    if (localStorage.getItem("imageNum") == 0)
    {
        document.getElementById("previous").style.display = "none";
        document.getElementById("labelPrevious").style.display = "none";
    }
}

function changeCaption(){
    document.getElementById("changeCaptionDiv").style.display = "block";
    var retrievedDataimageName = localStorage.getItem("imageName");
    var imageNameArray = JSON.parse(retrievedDataimageName);
    document.getElementById("ChangeCaptionImageName").value = imageNameArray[localStorage.getItem("imageNum")];

}

function changeGeolocation(){
    document.getElementById("changeGeolocationDiv").style.display = "block";
    var retrievedDataimageName = localStorage.getItem("imageName");
    var imageNameArray = JSON.parse(retrievedDataimageName);
    document.getElementById("ChangeGeolocationImageName").value = imageNameArray[localStorage.getItem("imageNum")];
}


function updateCaption(){
    var text = document.getElementById("ChangeCaption").value;
    localStorage.setItem("caption", text);
    var retrievedDataCaption = localStorage.getItem("captionArray");
    var captionArray = JSON.parse(retrievedDataCaption);
    captionArray[localStorage.getItem("imageNum")] = text;
    localStorage.setItem("captionArray", JSON.stringify(captionArray));
    document.getElementById("caption").innerText =captionArray[localStorage.getItem("imageNum")];
    document.getElementById("changeCaptionDiv").style.display = "none";

}

function updateGeolocation(){
    var text = document.getElementById("ChangeGeolocation").value;
    localStorage.setItem("geoLocation", text);
    var retrievedDataimageName = localStorage.getItem("geoLocation");
    var imageNameArray = JSON.parse(retrievedDataimageName);
    imageNameArray[localStorage.getItem("imageNum")] = text;
    localStorage.setItem("imageNameArray", JSON.stringify(imageNameArray));
    document.getElementById("geoLocation").innerText =captionArray[localStorage.getItem("imageNum")];
    document.getElementById("changeGeolocationDiv").style.display = "none";

}


function cancelAccount(){
    document.getElementById("changeCaptionDiv").style.display = "none";
    document.getElementById("shareImageDiv").style.display = "none";
    document.getElementById("searchByName").style.display = "none";
    document.getElementById("DeleteImageDiv").style.display = "none";
    document.getElementById("changeGeolocationDiv").style.display = "none";
}

function share(){
    document.getElementById("shareImageDiv").style.display = "block";
    var retrievedDataimageName = localStorage.getItem("imageName");
    var imageNameArray = JSON.parse(retrievedDataimageName);
    document.getElementById("ChangeImageNameShare").value = imageNameArray[localStorage.getItem("imageNum")];
    document.getElementById("imageToShare").value = "C:/Users/humai/downloads/" + imageNameArray[localStorage.getItem("imageNum")];
}

function validateFormShare(){
    if (((document.getElementById("emailToShare").value) == ""))
    {
        alert("Enter an email");
        return false;
    }
}

function validateFormDelete(){
    console.log(imageNameArray[localStorage.getItem("imageNum")]);
}

function searchByName(){
    document.getElementById("searchByName").style.display = "block";
}

function deleteImageClick(){
    document.getElementById("DeleteImageDiv").style.display = "block";
    var retrievedDataimageName = localStorage.getItem("imageName");
    var imageNameArray = JSON.parse(retrievedDataimageName);
    console.log(imageNameArray[localStorage.getItem("imageNum")]);
    document.getElementById("deleteInput").value = imageNameArray[localStorage.getItem("imageNum")];
}

var loadFile = function(event) {
	var image =  URL.createObjectURL(event.target.files[0]);
	var imageNameUploaded = (document.getElementById("file").value).slice(12);
	document.getElementById("PicturePath").value= "C:/Users/humai/downloads/" + imageNameUploaded;
	document.getElementById("ImageNameUpload").value= imageNameUploaded;
};
