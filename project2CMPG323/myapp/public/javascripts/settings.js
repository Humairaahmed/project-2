/* display the name and surname of the user */
window.addEventListener('load', (event) => {
    //Settings the firstname variable
    var nameFromBackEnd= document.querySelector("#Name").textContent;
    localStorage.setItem("firstName", nameFromBackEnd);

    //Setting the surname variable
    var surnameFromBackend= document.querySelector("#Surname").textContent;
    localStorage.setItem("lastName", surnameFromBackend);

    //Settings the phoneNumber variable
    var phoneNumberFromBackEnd= document.querySelector("#Number").textContent;
    localStorage.setItem("phoneNumber", phoneNumberFromBackEnd);

    //Settings the bio variable
    var bioFromBackEnd= document.querySelector("#Bio").textContent;
    localStorage.setItem("bio", bioFromBackEnd);

    //Settings the bio variable
    var memberOfFromBackEnd= document.querySelector("#MemberOf").textContent;
    localStorage.setItem("memberOf", memberOfFromBackEnd);

    document.getElementById("nameAndSurname").innerText = test + " " + localStorage.getItem("lastName") ;

});

//Display personalInformation div
function displayPersonalInfo() {
    document.getElementById("personalInfoDiv").style.display = "block";
    document.getElementById("accountSettingsDiv").style.display = "none";
    document.getElementById("profileDivID").style.display = "none";
    document.getElementById("name").value = localStorage.getItem("firstName");
    document.getElementById("surname").value = localStorage.getItem("lastName");
    document.getElementById("email").value = localStorage.getItem("username");
    document.getElementById("number").value = localStorage.getItem("phoneNumber");
}

function update(){
    localStorage.setItem("firstName", document.getElementById("name").value);
    localStorage.setItem("lastName", document.getElementById("surname").value);
    localStorage.setItem("email", document.getElementById("email").value);
    localStorage.setItem("phoneNumber", document.getElementById("number").value);
    document.getElementById("name").value = localStorage.getItem("firstName");
    document.getElementById("surname").value = localStorage.getItem("lastName");
    document.getElementById("email").value = localStorage.getItem("username");
    document.getElementById("number").value = localStorage.getItem("phoneNumber");
}

//Display profile div
function displayProfile() {
    document.getElementById("personalInfoDiv").style.display = "none";
    document.getElementById("accountSettingsDiv").style.display = "block";
    document.getElementById("profileDivID").style.display = "none";
    document.getElementById("usernameForProfile").value = localStorage.getItem("username");
    document.getElementById("memberOf").value = localStorage.getItem("memberOf");
}

//display account settings div
function displayAccountSettings() {
    document.getElementById("personalInfoDiv").style.display = "none";
    document.getElementById("accountSettingsDiv").style.display = "none";
    document.getElementById("profileDivID").style.display = "block";
    document.getElementById("emailUsername").value = localStorage.getItem("username");
    document.getElementById("bioBox").value = localStorage.getItem("bio");
}

/* sign out and clearing all local variables */
function signOut() {
    localStorage.setItem("firstName","");
    localStorage.setItem("lastName", "");
    localStorage.setItem("email", "");
    localStorage.setItem("phoneNumber", "");
}

