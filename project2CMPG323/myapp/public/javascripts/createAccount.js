/* save all input in local variables */
function createAccount(){
    localStorage.setItem("username", document.getElementById("email").value);
    localStorage.setItem("firstName", document.getElementById("Name").value);
    localStorage.setItem("lastName", document.getElementById("lastName").value);
    localStorage.setItem("phoneNumber", document.getElementById("phoneNumber").value);
}

function validateFormCreateAccount() {
    //Checking if the user has all inputs
    if (((document.getElementById("email").value) != ""))
    {
        if (((document.getElementById("password").value) != ""))
        {
            if (((document.getElementById("confirmPassword").value) != ""))
            {
                if (((document.getElementById("Name").value) != ""))
                {
                    if (((document.getElementById("lastName").value) != ""))
                    {
                        if (((document.getElementById("phoneNumber").value) != ""))
                        {
                            //Checking the password equals the confirm password
                            if((document.getElementById("password").value) == (document.getElementById("confirmPassword").value)){
                                return true;
                            }
                            else
                            {
                                alert("Password and confirm passwords are not the same ");
                                return false;
                            }

                        }
                    }
                }
            }
        }
    }
    alert("Please fill in all fields");
    return false;
}