var express = require('express');
var bodyParser = require('body-parser');
var router = express.Router();

const app = express();

//Hashing the password
const bcrypt = require('C:/Users/humai/Desktop/custom-bcrypt');

var username = "";
var password;

var picturePath = "";
var pictureToDelete = "";
var correctpassword = "false";

//User information
var firstName = "";
var lastName = "";
var phoneNumber = "";

//imageInfo
imageName = "";
caption = "";
sharedStatus = "";
datePosted = "";
sharedWith = "";
geolocation = "";



//Account information
var bio = "";
var memberOf = "";

var share = "";

var captionInput = 0;

//name of bucket
var bucketinS3 = "savingimagesfortheuser";
var k;
var imagesForTheUser = [];
var imagesForTheUser1 = [];
var captionsArray = [];

const visited = []

/*  Update caption */
router.post("/ChangeCaption", (req, res) =>{
    //Read the values from the form
    caption = `${req.body.NewCaption}`;
    imageName = `${req.body.imagename}`;

    //updating the array that fold all information about the images and the user
    for (var i = 0 ; i < k ; i++)
    {
        //Look for the image, search by the name
        if(imagesForTheUser[i] === imageName)
        {
            console.log("found the image");
        }
        //if the image is found, change the caption
        if(imagesForTheUser[i-1] === imageName)
        {
            imagesForTheUser[i] = caption;
            console.log("changed the caption");
            // Update the caption in the database
            const oracledb = require('oracledb')
            const config = {
              user: 'HUMAIRA',
              password: 'cmpg311',
              connectString : "localhost:1521/xe"
            }

            async function UpdateCaption() {
              let conn
              try {
                conn = await oracledb.getConnection(config)
                const result = await conn.execute(
            		"UPDATE imageinfo SET caption = '" + caption + "' WHERE imagename = '" + imageName + "'"
                )
                console.log(result);
            	conn.commit();

              } catch (err) {
                console.log('Ouch!', err)
              } finally {
                if (conn) { // conn assignment worked, need to close
                  await conn.close();
                }
              }
            }

            UpdateCaption(101);
        }
    }
    res.render('account', { images: imagesForTheUser});
});


/* Update the geolocation for the image */
router.post("/ChangeGeolocation", (req, res) =>{
    //Read the values from the form
    geolocation = `${req.body.NewGeolocation}`;
    imageName = `${req.body.imagename}`;

    for (var i = 0 ; i < k ; i++)
    {
        if(imagesForTheUser[i] === imageName)
        {
            console.log("found the image");
        }
        if(imagesForTheUser[i-5] === imageName)
        {
            imagesForTheUser[i] = geolocation;
            console.log("changed the geolocation");
            const oracledb = require('oracledb')
            const config = {
              user: 'HUMAIRA',
              password: 'cmpg311',
              connectString : "localhost:1521/xe"
            }

            // Update the caption in the database
            async function UpdateGeolocation() {
              let conn
              try {
                conn = await oracledb.getConnection(config)
                const result = await conn.execute(
            		"UPDATE imageinfo SET geolocation = '" + geolocation + "' WHERE imagename = '" + imageName + "'"
                )
                console.log(result);
            	conn.commit();

              } catch (err) {
                console.log('Ouch!', err)
              } finally {
                if (conn) { // conn assignment worked, need to close
                  await conn.close();
                }
              }
            }

            UpdateGeolocation(101);
        }
    }
    console.log(imagesForTheUser);
    res.render('account', { images: imagesForTheUser});
});


/* Share an image with another user*/
router.post('/Share', (req, res) => {
    share = `${req.body.EmailToShare}`;
    imageName = `${req.body.UpdateSharedStatus}`;

    //getting input of the path of the picture
    picturePath = `${req.body.imageToShare}`;
    // Load the AWS SDK for Node.js
    var AWS = require('aws-sdk');
    // Set the region
    AWS.config.update({region: 'us-east-1'});

    // Create S3 service object
    var s3 = new AWS.S3({apiVersion: '2006-03-01'});

    // call S3 to retrieve upload file to specified bucket
    process.argv[2] = bucketinS3 + "/" + share;
    process.argv[3] = picturePath;
    var uploadParams = {Bucket: process.argv[2], Key: '', Body: ''};
    var file = process.argv[3];

    // Configure the file stream and obtain the upload parameters
    var fs = require('fs');
    var fileStream = fs.createReadStream(file);
    fileStream.on('error', function(err) {
        console.log('File Error', err);
    });
    uploadParams.Body = fileStream;
    var path = require('path');
    uploadParams.Key = path.basename(file);

    // call S3 to retrieve upload file to specified bucket
    s3.upload (uploadParams, function (err, data) {
        if (err) {
            console.log("Error", err);
        } if (data) {
            console.log("Upload Success", data.Location);
            var sharedAccounts = "";
            for (var i = 0 ; i < k ; i++)
            {
                if(imagesForTheUser[i] === imageName)
                {
                    console.log("found the image");
                    caption = imagesForTheUser[i+1];
                    postedBy = "Posted by " + username;
                    shared = "not shared";
                    var PathOfPicture = "https://savingimagesfortheuser.s3.amazonaws.com/" + share + "/" + imageName;
                    geolocation = imagesForTheUser[i+5];
                    console.log(geolocation);
                    if (imagesForTheUser[i+4] === "not shared")
                    {
                        console.log("empty");
                        imagesForTheUser[i+4] = "Shared with:\n  " + share;
                        sharedAccounts = share;
                    }
                    else{
                        console.log(share);
                        sharedAccounts = imagesForTheUser[i+4] + ";" + share;
                        imagesForTheUser[i+4] = "Shared with:\n  " + share + "\n   " + imagesForTheUser[i+4];
                    }
                    const oracledb = require('oracledb')
                    const config = {
                      user: 'HUMAIRA',
                      password: 'cmpg311',
                      connectString : "localhost:1521/xe"
                    }

                    //Updating the database sharedaccounts column in accountinfo
                    async function UpdateSharedAccounts() {
                      let conn
                      try {
                        conn = await oracledb.getConnection(config)
                        const result = await conn.execute(
                            "UPDATE imageinfo SET sharedaccounts = '" + sharedAccounts + "' WHERE imagename = '" + imageName + "'"
                        )
                        conn.commit();
                      } catch (err) {
                        console.log('Ouch!', err)
                      } finally {
                        if (conn) { // conn assignment worked, need to close
                          await conn.close();
                        }
                      }
                    }

                    UpdateSharedAccounts(101);

                    // Updating the imageinfo
                    async function AddTheSharedImageToimageinfoDatabase() {
                      let conn
                      try {
                        conn = await oracledb.getConnection(config)
                        const result = await conn.execute(
                            "INSERT INTO imageinfo VALUES('" + share + "','" + imageName + "','" + caption + "','" + postedBy + "', TO_DATE('2003/05/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'), '" + 'not shared' + "','" + PathOfPicture + "','" + geolocation + "')"
                        )
                        console.log(result);
                        conn.commit();

                      } catch (err) {
                        console.log('Ouch!', err)
                      } finally {
                        if (conn) { // conn assignment worked, need to close
                          await conn.close();
                        }
                      }
                    }

                    AddTheSharedImageToimageinfoDatabase(101);
                    }
                }
            }
          });
    res.render('account', { });
});


/* Deleting an image  */
router.post('/account', (req, res) => {
    pictureToDelete = `${req.body.pictureToDelete}`;
    // Load the AWS SDK for Node.js
        var AWS = require('aws-sdk');
        // Set the region
        AWS.config.update({region: 'us-east-1'});
        var s3 = new AWS.S3({apiVersion: '2006-03-01'});

        process.argv[2] = bucketinS3 + "/" + username ;
        var uploadParams = {Bucket: process.argv[2], Key: pictureToDelete};
        console.log(uploadParams);

         s3.deleteObject (uploadParams, function (err, data) {
                 if (err) {
                     console.log("Error", err);
                 } if (data) {
                     console.log("image deleted");
                     for (var i = 0 ; i < k ; i++)
                     {
                         if(imagesForTheUser[i+1] === pictureToDelete)
                         {
                            var startDeleteHere = i;
                            console.log("Start here " + i);
                         }
                     }
                     imagesForTheUser.splice(startDeleteHere,7);
                     k = k-7;
                     console.log(k);
                     console.log(imagesForTheUser);

                     const oracledb = require('oracledb')
                     const config = {
                       user: 'HUMAIRA',
                       password: 'cmpg311',
                       connectString : "localhost:1521/xe"
                     }

                    // Remove the metadata in the database for the image, in imageinfo
                     async function DeleteMetaData() {
                       let conn
                       try {
                         conn = await oracledb.getConnection(config)
                         const result = await conn.execute(
                     		"DELETE imageinfo WHERE imagename = '" + pictureToDelete  + "'"
                         )
                         console.log(result);
                     	conn.commit();

                       } catch (err) {
                         console.log('Ouch!', err)
                       } finally {
                         if (conn) { // conn assignment worked, need to close
                           await conn.close();
                         }
                       }
                     }

                     DeleteMetaData(101);
                 }
         });
        res.render('account', { });

});


/* Deleting the folder in the s3 bucket and Delete the account by deleting the information in accountinfo
     -the folder first needs to be empty */
router.post('/settings', (req, res) => {
    // Load the AWS SDK for Node.js
    var AWS = require('aws-sdk');
    // Set the region
    AWS.config.update({region: 'us-east-1'});
    var s3 = new AWS.S3({apiVersion: '2006-03-01'});

    process.argv[2] = bucketinS3;
    var uploadParams = {Bucket:process.argv[2], Key: username +'/'};

    (async function() {
        const listParams = {
            Bucket: bucketinS3,
            Prefix: username
        };

        const listedObjects = await s3.listObjectsV2(listParams).promise();

        if (listedObjects.Contents.length === 0) return;

        const deleteParams = {
            Bucket: bucketinS3,
            Delete: { Objects: [] }
        };

        listedObjects.Contents.forEach(({ Key }) => {
            deleteParams.Delete.Objects.push({ Key });
        });

        await s3.deleteObjects(deleteParams).promise();

        if (listedObjects.IsTruncated) await emptyS3Directory(bucket, dir);
    })();
    const oracledb = require('oracledb')
    const config = {
      user: 'HUMAIRA',
      password: 'cmpg311',
      connectString : "localhost:1521/xe"
    }

    async function DeleteAccount() {
      let conn
      try {
        conn = await oracledb.getConnection(config)
        const result = await conn.execute(
    		"DELETE accountinfo WHERE username = '" + username + "'"
        )
        console.log(result);
    	conn.commit();

      } catch (err) {
        console.log('Ouch!', err)
      } finally {
        if (conn) { // conn assignment worked, need to close
          await conn.close();
        }
      }
    }

    DeleteAccount(101);

    async function DeleteAccountImageinfo() {
      let conn
      try {
        conn = await oracledb.getConnection(config)
        const result = await conn.execute(
    		"DELETE imageinfo WHERE username = '" + username + "'"
        )
        console.log(result);
    	conn.commit();

      } catch (err) {
        console.log('Ouch!', err)
      } finally {
        if (conn) { // conn assignment worked, need to close
          await conn.close();
        }
      }
    }

    DeleteAccountImageinfo(101);

    async function DeleteAccountImageinfoShared() {
          let conn
          try {
            conn = await oracledb.getConnection(config)
            const result = await conn.execute(
        		"DELETE imageinfo WHERE sharedstatus = 'Posted by " + username + "'"
            )
            console.log(result);
        	conn.commit();

          } catch (err) {
            console.log('Ouch!', err)
          } finally {
            if (conn) { // conn assignment worked, need to close
              await conn.close();
            }
          }
        }

        DeleteAccountImageinfoShared(101);
    res.render('index', { });
});


/* Creating a folder in S3
    Creating an entry in the accountinfo database
*/
router.post('/createAccount', (req, res) => {
    //Saving input from the form into variables
    username = `${req.body.username}`;
    password = `${req.body.password}`;
    firstName = `${req.body.firstName}`;
    lastName = `${req.body.lastName}`;
    phoneNumber = `${req.body.phoneNumber}`;
    bio = "no bio";
    memberOf = "none";
    password = bcrypt.hash(password);

    k=0;
    imagesForTheUser = [];

    //Creating an S3 folder in savingimagesfortheuser bucket in AWS
    // Load the AWS SDK for Node.js
    var AWS = require('aws-sdk');
    // Set the region
    AWS.config.update({region: 'us-east-1'});
    var s3 = new AWS.S3({apiVersion: '2006-03-01'});

    process.argv[2] = bucketinS3;
    var uploadParams = {Bucket: process.argv[2], Key: username + '/', Body: ''};
    // call S3 to retrieve upload file to specified bucket
     s3.upload (uploadParams, function (err, data) {
        if (err) {
            console.log("Error", err);
        } if (data) {
            console.log("created folder");

            const oracledb = require('oracledb')
            const config = {
              user: 'HUMAIRA',
              password: 'cmpg311',
              connectString : "localhost:1521/xe"
            }

            async function CreateAccount() {
              let conn
              try {
                conn = await oracledb.getConnection(config)
                const result = await conn.execute(
            		"INSERT INTO accountinfo VALUES('" + username + "','" + password + "','" + firstName + "','" + lastName + "','" + phoneNumber + "','" + bio + "','" + memberOf + "')"
                )
                console.log(result);
            	conn.commit();

              } catch (err) {
                console.log('Ouch!', err)
              } finally {
                if (conn) { // conn assignment worked, need to close
                  await conn.close();
                }
              }
            }

            CreateAccount(101);
        }
     });

//directing to the account page
    res.render('account', { });

});


/* Sign in (check username and password)
    and list objects in the the users folder in the S3 bucket */
router.post('/', (req, res) => {
    //saving from the form
    username = `${req.body.username}`;
    password = `${req.body.password}`;

    const oracledb = require('oracledb')
    const config = {
      user: 'HUMAIRA',
      password: 'cmpg311',
      connectString : "localhost:1521/xe"
    }

    async function checkUsernameAndPassword() {
      let conn

      try {
        conn = await oracledb.getConnection(config)
        const result = await conn.execute(
    		"SELECT * FROM accountinfo WHERE username = '" + username + "'"
        )
        const result1 = await conn.execute(
            "SELECT * FROM imageinfo WHERE username = '" + username + "'"
        )
    	if (result.rows[0] == undefined)
    	{
    	    // Error handling for incorrect username or password
    		console.log("incorrect username or password");
    		var incorrectUsernameOrPassword = "incorrect username or password";
    		res.render('index', { images: incorrectUsernameOrPassword });
    	}
    	else{
    	    // Error handling to check if the hash passwords are equal
    	    if (bcrypt.compare(password,result.rows[0][1]) == true)
            {
                correctpassword = "true";
                console.log(bcrypt.compare(password,result.rows[0][1]));
                //Setting user information
                firstName = result.rows[0][2];
                lastName = result.rows[0][3];
                phoneNumber = result.rows[0][4];
                req.body.username = firstName;
                //Settings account information
                bio = result.rows[0][5];
                memberOf = result.rows[0][6];
                console.log("signed in");
                if (result1.rows[0] == undefined)
                {
                    console.log("No images");
                    k = 0;
                    imagesForTheUser = [];
                }
                else
                {
                    k=0;
                    imagesForTheUser = [];
                    var numberOfItems = "";
                    var l = 0;
                    while (numberOfItems != undefined)
                    {
                        numberOfItems = result1.rows[l];
                        l++;
                    }
                    l = l -2;
                    //Getting images from s3 for the user and storing into a text file
                    //text file called image.txt
                    // Load the AWS SDK for Node.js
                    var AWS = require('aws-sdk');
                    // Set the region
                    AWS.config.update({region: 'us-east-1'});
                    var s3 = new AWS.S3({apiVersion: '2006-03-01'});

                    //Reading in to image.txt
                    const fs = require('fs');
                    k = 0;
                    captionInput = 0;
                    (async function(){
                            try{
                                const response = await s3.listObjectsV2({
                                Bucket: bucketinS3,
                                Prefix: username
                            }).promise();
                            const textFile = 'C:/CMPG323-Project2/project-2/project2CMPG323/myapp/public/javascripts/image.txt';
                            fs.writeFileSync(textFile, '');
                            response.Contents.forEach(item => {
                                //saving to the text file if there is any images in the s3 folder
                                if (item.Key != (username + "/")){
                                    var image = "https://" + bucketinS3 +".s3.amazonaws.com/" + item.Key;
                                    fs.appendFileSync(textFile, image );
                                    fs.appendFileSync(textFile, '\n' );
                                    var num = 0;
                                    for (let i = 0 ; i <= l ; i++)
                                    {
                                        if ((result1.rows[i][6] === image))
                                        {
                                           num = i;
                                        }
                                    }

                                    imagesForTheUser[k] = image;
                                    k = k+1;

                                    imageName = result1.rows[num][1];
                                    imagesForTheUser[k] = imageName;
                                    k = k+1;

                                    caption = result1.rows[num][2];
                                    imagesForTheUser[k] = caption;
                                    captionsArray[captionInput] = caption;
                                    captionInput = captionInput+1;
                                    k = k+1;

                                    sharedStatus = result1.rows[num][3];
                                    imagesForTheUser[k] = sharedStatus;
                                    k = k+1;

                                    datePosted = result1.rows[num][4];
                                    imagesForTheUser[k] = datePosted;
                                    k = k+1;

                                    sharedWith = result1.rows[num][5];
                                    imagesForTheUser[k] = sharedWith;
                                    k = k+1;

                                    geolocation = result1.rows[num][7];
                                    imagesForTheUser[k] = geolocation;
                                    k = k+1;
                                }
                            });
                            } catch(e){
                                console.log("error" , e);
                            }
                    })();
                }
                   //directing to the account page
                   res.render('account', { images: imagesForTheUser });
            }
            else
            {
                console.log("incorrect password");
                var incorrectUsernameOrPassword = "incorrect username or password";
                res.render('index', { images: incorrectUsernameOrPassword });
            }
    	}
      } catch (err) {
        console.log('Ouch!', err)
      } finally {
        if (conn) { // conn assignment worked, need to close
          await conn.close();
        }
      }
    }

    checkUsernameAndPassword(101);


});

/* Uploading an image to S3
   adding all metadata to the database - imageinfo
*/
router.post('/home', (req, res) => {
    //getting input of the path of the picture
    picturePath = `${req.body.picturePath}`;
    caption = `${req.body.newImageCaption}`;
    geolocation = `${req.body.Geolocation}`;
    imageName = `${req.body.UploadImageName}`;


    // Load the AWS SDK for Node.js
    var AWS = require('aws-sdk');
    // Set the region
    AWS.config.update({region: 'us-east-1'});

    // Create S3 service object
    var s3 = new AWS.S3({apiVersion: '2006-03-01'});



    // call S3 to retrieve upload file to specified bucket
    process.argv[2] = bucketinS3 + "/" + username;
    process.argv[3] = picturePath;
    var uploadParams = {Bucket: process.argv[2], Key: '', Body: ''};
    var file = process.argv[3];

    // Configure the file stream and obtain the upload parameters
    var fs = require('fs');
    var fileStream = fs.createReadStream(file);
    fileStream.on('error', function(err) {
        console.log('File Error', err);
    });
    uploadParams.Body = fileStream;
    var path = require('path');
    uploadParams.Key = path.basename(file);

    // call S3 to retrieve upload file to specified bucket
    s3.upload (uploadParams, function (err, data) {
        if (err) {
            console.log("Error", err);
        } if (data) {
            console.log("Upload Success", data.Location);
            const textFile = 'C:/CMPG323-Project2/project-2/project2CMPG323/myapp/public/javascripts/image.txt';
            fs.appendFileSync(textFile, data.Location );
            fs.appendFileSync(textFile, '\n' );

            imagesForTheUser[k] = data.Location ;
            k = k+1;

            //var parts = data.Location.split("/");
            //var thePart = parts[4];
            //var thePart = "imageNameTest.jpg";
            //imageName = thePart;
            imagesForTheUser[k] = imageName;
            picturePath = "https://savingimagesfortheuser.s3.amazonaws.com/" + username + "/" + imageName;
            k = k+1;

            imagesForTheUser[k] = caption;
            captionsArray[captionInput] = caption;
            captionInput = captionInput+1;
            k = k+1;

            sharedStatus = "posted by " + username;
            imagesForTheUser[k] = sharedStatus;
            k = k+1;


            var today = new Date();
            var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
            var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
            var dateTime = date + " "+ time + " GMT+0200 (South Africa Standard Time)";
            datePosted = dateTime;
            imagesForTheUser[k] = datePosted;
            k = k+1;

            sharedWith = "not shared";
            imagesForTheUser[k] = sharedWith;
            k = k+1;

            imagesForTheUser[k] = geolocation;
            k = k+1;


            const oracledb = require('oracledb')
            const config = {
              user: 'HUMAIRA',
              password: 'cmpg311',
              connectString : "localhost:1521/xe"
            }

            async function DeleteMetaData() {
              let conn
              try {
                conn = await oracledb.getConnection(config)
                const result = await conn.execute(
            		"INSERT INTO imageinfo VALUES('" + username + "','" + imageName + "','" + caption + "','" + username + "', TO_DATE('2003/05/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'), '" + 'not shared' + "','" + picturePath + "','" + geolocation + "')"
                )
                console.log(result);
            	conn.commit();

              } catch (err) {
                console.log('Ouch!', err)
              } finally {
                if (conn) { // conn assignment worked, need to close
                  await conn.close();
                }
              }
            }

            DeleteMetaData(101);

            res.render('home', {images: imagesForTheUser });
        }
      });
});

/* GET sign in page. */
router.get('/', function(req, res, next) {
    res.render('index', { });
});

/* GET the settings page. */
router.get('/settings', function(req, res, next) {
    res.render('settings', { name: firstName, surname: lastName, number: phoneNumber, bio: bio, memberOf: memberOf});
});

/* GET the account page. */
router.get('/account', function(req, res, next) {
  res.render('account', {images: imagesForTheUser });
});

/* GET home page. */
router.get('/home', function(req, res, next) {
  res.render('home', {images: imagesForTheUser });
});

/* GET the create account page. */
router.get('/createAccount', function(req, res, next) {
  res.render('createAccount');
});


module.exports = router;
